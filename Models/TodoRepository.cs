using System;
using System.Collections.Generic;
using System.Linq;

namespace TodoApi.Models
{
    public class TodoRepository : ITodoRepository
    {
        private readonly TodoContext _context;
        public TodoRepository(TodoContext context)
        {
            _context = context;
        }

        public IEnumerable<TodoItem> GetAll(string userid)
        {
            int id = Int32.Parse(userid);
            return _context.TodoItems.Where(b => b.Owner == id).ToList();
        }

        public void Add(TodoItem item)
        {
            _context.TodoItems.Add(item);
            _context.SaveChanges();
        }

        public TodoItem Find(int key)
        {
            return _context.TodoItems.Find(key);
        }

        public TodoItem Remove(int key)
        {
            TodoItem item = _context.TodoItems.Find(key);
            _context.TodoItems.Remove(item);
            return item;
        }

        public void Update(TodoItem item)
        {
            _context.TodoItems.Update(item);
            _context.SaveChanges();
        }
    }
}
